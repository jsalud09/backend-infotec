<?php

use Illuminate\Database\Seeder;
use App\Cliente;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 50; $i++) {
            Cliente::create([
                'nombre' => $faker->name,
                'apellido' => $faker->firstname,
                'email' => $faker->unique()->safeEmail,
                'foto' => null,
            ]);
        }
    }
}
