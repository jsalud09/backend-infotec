<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        return response()->json([
            'data'=> $clientes,            
            'status' => 'success'            
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $cliente = Cliente::findOrFail($id);                
        return response()->json([
            'data'=> $cliente,            
            'status' => 'success'            
        ],200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $cliente = new Cliente;       
        $cliente->nombre = $request->nombre; 
        $cliente->apellido = $request->apellido;
        $cliente->email = $request->email;
        $file = $request->file("file");
        if(!empty($file)){   
            $nameFoto = "Image".uniqid().".".$file->getClientOriginalExtension();                
            $path = storage_path("app/public/")."images/";
            if($file->move($path,$nameFoto)){
                $cliente->foto = $nameFoto;
            }
        }
        if($cliente->save()){
            return response()->json([
                'data'=> $cliente,            
                'status' => 'success',            
                'message' => 'Cliente guardado exitosamente!!'
                ],200);
        }else{
            return response()->json([
                'data'=> null,            
                'status' => 'error',            
                'message' => 'Error al guardar cliente!!'
                ],200);
        }
              
    }    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::findOrFail($id);  
        $cliente->nombre = $request->nombre; 
        $cliente->apellido = $request->apellido;
        $cliente->email = $request->email;
        $file = $request->file("file");
        if(!empty($file)){   
            $nameFoto = "Image".uniqid().".".$file->getClientOriginalExtension();                
            $path = storage_path("app/public/")."images/";
            if($file->move($path,$nameFoto)){
                $cliente->foto = $nameFoto;
            }
        }
        if($cliente->save()){
            return response()->json([
                'data'=> $cliente,            
                'status' => 'success',            
                'message' => 'Cliente editado exitosamente!!'
                ],200);
        }else{
            return response()->json([
                'data'=> null,            
                'status' => 'error',            
                'message' => 'Error al editar cliente!!'
                ],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);                 
        if(!is_null($cliente)){
            $cliente->delete();             
            return response()->json([
                'data'=> $cliente,            
                'status' => 'success',
                'message' => 'Cliente eliminado exitosamente!!'             
            ],200);
        }else{
            return response()->json([
                'data'=> $cliente,            
                'status' => 'error',
                'message' => 'Cliente no encontrado!!'             
            ],200);
        }          
    }

}
