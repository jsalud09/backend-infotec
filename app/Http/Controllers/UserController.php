<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    public function getAll(Request $request){
        $users = User::all();
        return response()->json([
            'data'=> $users,            
            'status' => 'success',
            'message' => 'Bienvenido al sistema!!'
        ],200);
    }

    public function login(Request $request){
    
        $user = User::whereEmail($request->email)->first();        
        if(!is_null($user) && \Hash::check($request->password, $user->password)){
            $token = $user->createToken('infotec')->accessToken;
            return response()->json([
                'data'=> $user,
                'token' => $token,
                'status' => 'success',
                'message' => 'Bienvenido al sistema!!'
            ],200);
        }else{
            return response()->json([
                'data'=> null,
                'status' => 'error',
                'message' => 'Usuario no econtrado!!'
            ],200);
        }
    }

    public function logout(){
        $user = \auth()->user();
        $user->tokens->each(function($token,$key){
            $token->delete();
        });
        $user->save();

        return response()->json([
            'status' => 'success',
            'message'=> 'Sesion finalizada'
        ],200);
    }
}
